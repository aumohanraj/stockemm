package com.he.stockemM.db.model;

import android.content.Context;


public class Tax {
	
	
	int tax_id=0;
	String tax_description="";
	Double tax_percentage=0.00;
	String type="";
	String tax_enabled="";
	String domain_id="";
	
	Context context;

	public Tax(Context context) {
		this.context=context;
	}
	
	public int getTax_id() {
		return tax_id;
	}

	public void setTax_id(int tax_id) {
		this.tax_id = tax_id;
	}

	public String getTax_description() {
		return tax_description;
	}

	public void setTax_description(String tax_description) {
		this.tax_description = tax_description;
	}

	public Double getTax_percentage() {
		return tax_percentage;
	}

	public void setTax_percentage(Double tax_percentage) {
		this.tax_percentage = tax_percentage;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTax_enabled() {
		return tax_enabled;
	}

	public void setTax_enabled(String tax_enabled) {
		this.tax_enabled = tax_enabled;
	}

	public String getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(String domain_id) {
		this.domain_id = domain_id;
	}
}