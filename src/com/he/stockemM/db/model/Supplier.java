package com.he.stockemM.db.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.he.stockemM.lib.DatabaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


public class Supplier {
	
	
	String id="";
	String domain_id="";
	String name="";
	String street_address="";
	String street_address2="";
	String city="";
	String state="";
	String zip_code="";
	String country="";
	String phone="";
	String mobile_phone="";
	String fax="";
	String email="";
	String logo="";
	String footer="";
	String paypal_business_name="";
	String paypal_notify_url="";
	String paypal_return_url="";
	String eway_customer_id="";
	String notes="";
	String custom_field1="";
	String custom_field2="";
	String custom_field3="";
	String custom_field4="";
	String enabled="";
	Context context;

	public Supplier(Context context) {
		context=this.context;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(String domain_id) {
		this.domain_id = domain_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet_address() {
		return street_address;
	}

	public void setStreet_address(String street_address) {
		this.street_address = street_address;
	}

	public String getStreet_address2() {
		return street_address2;
	}

	public void setStreet_address2(String street_address2) {
		this.street_address2 = street_address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getPaypal_business_name() {
		return paypal_business_name;
	}

	public void setPaypal_business_name(String paypal_business_name) {
		this.paypal_business_name = paypal_business_name;
	}

	public String getPaypal_notify_url() {
		return paypal_notify_url;
	}

	public void setPaypal_notify_url(String paypal_notify_url) {
		this.paypal_notify_url = paypal_notify_url;
	}

	public String getPaypal_return_url() {
		return paypal_return_url;
	}

	public void setPaypal_return_url(String paypal_return_url) {
		this.paypal_return_url = paypal_return_url;
	}

	public String getEway_customer_id() {
		return eway_customer_id;
	}

	public void setEway_customer_id(String eway_customer_id) {
		this.eway_customer_id = eway_customer_id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustom_field1() {
		return custom_field1;
	}

	public void setCustom_field1(String custom_field1) {
		this.custom_field1 = custom_field1;
	}

	public String getCustom_field2() {
		return custom_field2;
	}

	public void setCustom_field2(String custom_field2) {
		this.custom_field2 = custom_field2;
	}

	public String getCustom_field3() {
		return custom_field3;
	}

	public void setCustom_field3(String custom_field3) {
		this.custom_field3 = custom_field3;
	}

	public String getCustom_field4() {
		return custom_field4;
	}

	public void setCustom_field4(String custom_field4) {
		this.custom_field4 = custom_field4;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
public String save(){
		
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();		
		
		values.put(DatabaseHelper.KEY_SUPPLIER_CITY,this.city);
		values.put(DatabaseHelper.KEY_SUPPLIER_COUNTRY,this.country);
		values.put(DatabaseHelper.KEY_SUPPLIER_CUSTOM_FIELD1,this.custom_field1);
		values.put(DatabaseHelper.KEY_SUPPLIER_CUSTOM_FIELD2,this.custom_field2);
		values.put(DatabaseHelper.KEY_SUPPLIER_CUSTOM_FIELD3,this.custom_field3);
		values.put(DatabaseHelper.KEY_SUPPLIER_CUSTOM_FIELD4,this.custom_field4);
		values.put(DatabaseHelper.KEY_SUPPLIER_DOMAIN_ID,this.domain_id);
		values.put(DatabaseHelper.KEY_SUPPLIER_EMAIL,this.email);
		values.put(DatabaseHelper.KEY_SUPPLIER_ENABLED,this.enabled);
		values.put(DatabaseHelper.KEY_SUPPLIER_EWAY_CUSTOMER_ID,this.eway_customer_id);
		values.put(DatabaseHelper.KEY_SUPPLIER_FAX,this.fax);
		values.put(DatabaseHelper.KEY_SUPPLIER_FOOTER,this.footer);
		values.put(DatabaseHelper.KEY_SUPPLIER_LOGO,this.logo);
		values.put(DatabaseHelper.KEY_SUPPLIER_MOBILE_PHONE,this.mobile_phone);
		values.put(DatabaseHelper.KEY_SUPPLIER_NAME,this.name);
		values.put(DatabaseHelper.KEY_SUPPLIER_NOTES,this.notes);
		values.put(DatabaseHelper.KEY_SUPPLIER_PAYPAL_BUSINESS_NAME,this.paypal_business_name);
		values.put(DatabaseHelper.KEY_SUPPLIER_PAYPAL_NOTIFY_URL,this.paypal_notify_url);
		values.put(DatabaseHelper.KEY_SUPPLIER_PAYPAL_RETURN_URL,this.paypal_return_url);
		values.put(DatabaseHelper.KEY_SUPPLIER_PHONE,this.phone);
		values.put(DatabaseHelper.KEY_SUPPLIER_STATE,this.state);
		values.put(DatabaseHelper.KEY_SUPPLIER_STREET_ADDRESS,this.street_address);
		values.put(DatabaseHelper.KEY_SUPPLIER_STREET_ADDRESS2,this.street_address2);
		values.put(DatabaseHelper.KEY_SUPPLIER_ZIP_CODE,this.zip_code);
		
		if(this.id.contentEquals("")){
			this.id= String.valueOf(db.insert(DatabaseHelper.TABLE_SUPPLIER, null, values));
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_SUPPLIER, values, "id=?", new String[] {String.valueOf(this.id)});
			return this.id;
		}
	}
	
	
	

}

