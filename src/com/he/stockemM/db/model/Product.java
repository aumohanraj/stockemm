package com.he.stockemM.db.model;

import android.content.Context;


public class Product {
	
	String id="";
	String domain_id="";
	String description="";
	double unit_price=0.00;
	int default_tax_id=0;
	int default_tax_id_2=0;
	double cost=0.00;
	String reorder_level="";
	String custom_field1="";
	String custom_field2="";
	String custom_field3="";
	String custom_field4="";
	String notes="";
	String enabled="";
	String visible="";
	String supplier="";
	String manufacturer="";
	
	Context context;

	public Product(Context context) {
		this.context=context;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(String domain_id) {
		this.domain_id = domain_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(double unit_price) {
		this.unit_price = unit_price;
	}

	public int getDefault_tax_id() {
		return default_tax_id;
	}

	public void setDefault_tax_id(int default_tax_id) {
		this.default_tax_id = default_tax_id;
	}

	public int getDefault_tax_id_2() {
		return default_tax_id_2;
	}

	public void setDefault_tax_id_2(int default_tax_id_2) {
		this.default_tax_id_2 = default_tax_id_2;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String getReorder_level() {
		return reorder_level;
	}

	public void setReorder_level(String reorder_level) {
		this.reorder_level = reorder_level;
	}

	public String getCustom_field1() {
		return custom_field1;
	}

	public void setCustom_field1(String custom_field1) {
		this.custom_field1 = custom_field1;
	}

	public String getCustom_field2() {
		return custom_field2;
	}

	public void setCustom_field2(String custom_field2) {
		this.custom_field2 = custom_field2;
	}

	public String getCustom_field3() {
		return custom_field3;
	}

	public void setCustom_field3(String custom_field3) {
		this.custom_field3 = custom_field3;
	}

	public String getCustom_field4() {
		return custom_field4;
	}

	public void setCustom_field4(String custom_field4) {
		this.custom_field4 = custom_field4;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getSupplier() {
		return supplier;
	}
	
	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public String getDisplayName(){
		String displayName =this.description;
		if(!this.custom_field1.trim().isEmpty()){
			displayName = displayName+","+this.custom_field1;
		}		
		if(!this.custom_field2.trim().isEmpty()){
			displayName = displayName+","+this.custom_field2;
		}
		return displayName;
	}
}