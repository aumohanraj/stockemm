package com.he.stockemM.payments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.he.stockemM.R;
import com.he.stockemM.db.model.Payment;

public class OfflinePaymentFragment extends Fragment{
	
	public OfflinePaymentFragment(){}
	EditText etOfflinePaymentInvoiceNo, etOfflinePaymentAmount;
	Button btnSave;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_offline_payments, container, false);
        etOfflinePaymentInvoiceNo = (EditText)rootView.findViewById(R.id.etOfflinePaymentInvoiceNo);
        etOfflinePaymentAmount= (EditText)rootView.findViewById(R.id.etOfflinePaymentInvoiceAmount);
        btnSave = (Button)rootView.findViewById(R.id.btnOfflinePaymentSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(etOfflinePaymentInvoiceNo.getText().toString().isEmpty() || etOfflinePaymentAmount.getText().toString().isEmpty()){
					Toast.makeText(v.getContext(), "Please fill all the fields", 10).show();
					return;
				}
				
				Payment payment = new Payment(v.getContext());
				payment.setAc_amount(etOfflinePaymentAmount.getText().toString());
				payment.setAc_inv_id(etOfflinePaymentInvoiceNo.getText().toString());
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();				
				payment.setAc_date(dateFormat.format(date));
				payment.setAc_notes("Offline Mobile Payment");
				payment.save();
				Toast.makeText(v.getContext(), "Payment saved successfully for InvoiceNo:"+etOfflinePaymentInvoiceNo.getText().toString(), 10).show();
				etOfflinePaymentInvoiceNo.setText("");
				etOfflinePaymentAmount.setText("");
				
				
				
			}
		});
        
        return rootView;
    }
}
