package com.he.stockemM.lib;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.he.stockemM.R;

public class CustomListAdapter extends BaseAdapter implements Filterable{
	private Activity activity;
	private LayoutInflater inflater;
	private List<CustomListItem> ListItems;
	private List<CustomListItem> OriginalData;
	private ItemFilter mFilter = new ItemFilter();

	public CustomListAdapter(Activity activity, List<CustomListItem> ListItems) {
		this.activity = activity;
		this.ListItems = ListItems;
		this.OriginalData = ListItems;
	}

	@Override
	public int getCount() {
		return ListItems.size();
	}

	@Override
	public Object getItem(int location) {
		return ListItems.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.list_row, null);
		
		TextView title = (TextView) convertView.findViewById(R.id.title);
		TextView list_description = (TextView) convertView.findViewById(R.id.list_description);
		TextView list_footer_left = (TextView) convertView.findViewById(R.id.list_footer_left);
		TextView list_footer_right = (TextView) convertView.findViewById(R.id.list_footer_right);

		// getting movie data for the row
		CustomListItem item = ListItems.get(position);		
		
		title.setText(item.getTitle());		
		list_description.setText(item.getDescription());				
		list_footer_left.setText(item.getFooterLeft());		
		list_footer_right.setText(String.valueOf(item.getFooterRight()));
		return convertView;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		return mFilter;
	}
	
	private class ItemFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			
			String filterString = constraint.toString().toLowerCase();
			
			FilterResults results = new FilterResults();
			
			final List<CustomListItem> list = OriginalData;

			int count = list.size();
			final ArrayList<CustomListItem> nlist = new ArrayList<CustomListItem>(count);

			String filterableString ;
			
			for (int i = 0; i < count; i++) {
				filterableString = list.get(i).getTitle().toLowerCase();
				if (filterableString.toLowerCase().contains(filterString)) {
					nlist.add(list.get(i));
				}
			}
			
			results.values = nlist;
			results.count = nlist.size();

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			ListItems = (ArrayList<CustomListItem>) results.values;
			notifyDataSetChanged();
		}

	}
	
	public void setOriginalData(List<CustomListItem> original){
		this.OriginalData = original;
		this.ListItems = original;
	}

}