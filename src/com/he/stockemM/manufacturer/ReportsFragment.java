package com.he.stockemM.manufacturer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockemM.R;
import com.he.stockemM.lib.AsyncRequest;
import com.he.stockemM.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockemM.lib.AutoCompleteItem;
import com.he.stockemM.lib.AutocompleteArrayAdapter;

public class ReportsFragment extends Fragment implements View.OnClickListener, OnAsyncRequestComplete {
	
	public ReportsFragment(){}
	String FiltersJSON, distributorID;
	Button btnPrimary, btnSecondary, btnStock, btnConsolidate, btnDownload, btnOrders, btnReportSubmit;
	ImageButton setStartDate, setEndDate;
	LinearLayout FilterBrand, FilterProduct, FilterDateRange, FilterRoute, FilterCustomer;
	TextView TVStartDate, TVEndDate, date;
	ArrayAdapter<AutoCompleteItem> productAdapter;
	int selectedProductID=0;
	AutoCompleteTextView acProducts, acCustomers;
	ArrayAdapter<AutoCompleteItem> customerAdapter;
	int selectedCustomerID=0;
	String reportName, daterange_start, daterange_end, apiname;
	AutoCompleteItem selectedCustomerItem, selectedProductItem;
	Spinner spinBrand, spinRoute;
	int scrWidth, scrWidth80by3, scrWidth10;
    Button btnMenu[];
    LinearLayout llRoot, llHsMain, llMenuGroup1, llMenuGroup2, llMenuGroup3;
    Context context;
    HorizontalScrollView hScrollView;
	
    List<String> listBrand = new ArrayList<String>();
	List<String> listBrandID = new ArrayList<String>();
	
	List<String> listRoutes = new ArrayList<String>();
	List<String> listRoutesID = new ArrayList<String>();
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);
         
        llRoot = (LinearLayout) rootView.findViewById(R.id.TopMenuReportsRoot);
        DisplayMetrics dm = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        scrWidth = dm.widthPixels;
        int scrWidth1 = dm.widthPixels;
        
        scrWidth = (int) (0.74 * scrWidth); // Calculating 80 % Width of Screen for Horizontal Scroll View
        // Calulation 1/3 of Width of  Horizontal Scroll View for 3  menu items
       if(scrWidth1 <500){
       	scrWidth80by3 = (int) (scrWidth / 1.5);
       }else if(scrWidth1 >=500 && scrWidth1 <=800){
       	scrWidth80by3 = (int) (scrWidth / 2);
       }else{
       	scrWidth80by3 = 300;
       }
       
       scrWidth10 = (int) (0.13 * dm.widthPixels);// Calulation 10% Width of Screen for Previous And Next Button

       context = this.getActivity();

       btnMenu = new Button[9];

       llMenuGroup1 = new LinearLayout(context);
       llMenuGroup2 = new LinearLayout(context);
       llMenuGroup3 = new LinearLayout(context);

       llMenuGroup1.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llMenuGroup2.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llMenuGroup3.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llHsMain = new LinearLayout(context);
       llHsMain.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));

       TVStartDate = (TextView)rootView.findViewById(R.id.TVStartDate);
       TVStartDate.setText(getStartDateOfMonth());
       TVEndDate = (TextView)rootView.findViewById(R.id.TVEndDate);
       TVEndDate.setText(getEndDateOfMonth());
       
       spinBrand = (Spinner)rootView.findViewById(R.id.spinnerBrand);
       spinRoute = (Spinner)rootView.findViewById(R.id.spinnerRoute);
       
       setStartDate = (ImageButton)rootView.findViewById(R.id.setStartDate);
       setStartDate.setOnClickListener(new View.OnClickListener() {		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			date = TVStartDate;
			DialogFragment newFragment = new SelectDateFragment();
            newFragment.show(getFragmentManager(), "DatePicker");
		}
	});
       setEndDate = (ImageButton)rootView.findViewById(R.id.setEndDate);
       setEndDate.setOnClickListener(new View.OnClickListener() {		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			date = TVEndDate;
			DialogFragment newFragment = new SelectDateFragment();
            newFragment.show(getFragmentManager(), "DatePicker");
		}
	});
       
       acProducts = (AutoCompleteTextView)rootView.findViewById(R.id.acReportProduct);
       acCustomers = (AutoCompleteTextView)rootView.findViewById(R.id.acReportCustomer);
       btnReportSubmit = (Button)rootView.findViewById(R.id.btnReportSubmit);
       btnReportSubmit.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			if(!verifyDateRange()){				
				Toast.makeText(v.getContext(), "Invalid Date Rage", 10).show();
				return;
			}
			if(reportName.toLowerCase().contentEquals("primary")){
				getPrimary();				
			}
			
			if(reportName.toLowerCase().contentEquals("secondary")){				
				getSecondary();
			}
			
			
			
		}
	});
       btnPrimary = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnPrimary.setBackgroundResource(R.drawable.top_bar_button);
       btnPrimary.setId(1);        
       btnPrimary.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
       Drawable img = this.getActivity().getResources().getDrawable( R.drawable.ic_primary );
       img.setBounds( 0, 0, 60, 60 );
       btnPrimary.setCompoundDrawables( img, null, null, null );       
       btnPrimary.setText("Primary");       
       llHsMain.addView(btnPrimary);
       
       btnSecondary = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnSecondary.setBackgroundResource(R.drawable.top_bar_button);
       btnSecondary.setId(2);        
       btnSecondary.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
           
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_secondary );
       img.setBounds( 0, 0, 60, 60 );
       btnSecondary.setCompoundDrawables( img, null, null, null );
       btnSecondary.setText("Secondary");        
       llHsMain.addView(btnSecondary);
       
       btnStock = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnStock.setId(3);      
       btnStock.setBackgroundResource(R.drawable.top_bar_button);
       btnStock.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
       
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_stock );
       img.setBounds( 0, 0, 60, 60 );
       btnStock.setCompoundDrawables( img, null, null, null );
       btnStock.setText("Stock");        
       llHsMain.addView(btnStock);
       
       
       btnConsolidate = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnConsolidate.setId(4); 
       btnConsolidate.setBackgroundResource(R.drawable.top_bar_button);
       btnConsolidate.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));        
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_consolidated );
       img.setBounds( 0, 0, 60, 60 );
       btnConsolidate.setCompoundDrawables( img, null, null, null );
       btnConsolidate.setText("Consolidate");        
       llHsMain.addView(btnConsolidate);
       
       
       
       btnDownload = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnDownload.setId(5);     
       btnDownload.setBackgroundResource(R.drawable.top_bar_button);
       btnDownload.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));           
       btnDownload.setText("Download");
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_download );
       img.setBounds( 0, 0, 60, 60 );
       btnDownload.setCompoundDrawables( img, null, null, null );
       llHsMain.addView(btnDownload);
       btnDownload.setVisibility(View.GONE);
       btnOrders = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnOrders.setId(6);        
       btnOrders.setBackgroundResource(R.drawable.top_bar_button);
       btnOrders.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));        
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_order );
       img.setBounds( 0, 0, 60, 60 );
       btnOrders.setCompoundDrawables( img, null, null, null );
       btnOrders.setText("Orders");  
       llHsMain.addView(btnOrders);
       
       ImageButton btnNext = new ImageButton(context);
       btnNext.setOnClickListener(this);
       btnNext.setImageResource(R.drawable.ic_forward);
       btnNext.setId(0);
       
       ImageButton btnPre = new ImageButton(context);
       btnPre.setOnClickListener(this);
       btnPre.setImageResource(R.drawable.ic_back);
       btnPre.setPadding(3, 3, 3, 3);
       btnPre.setId(-1);
       
       btnNext.setLayoutParams(new LayoutParams(scrWidth10,
               LayoutParams.MATCH_PARENT));
       
       btnPre.setLayoutParams(new LayoutParams(scrWidth10,
               LayoutParams.MATCH_PARENT));
       
       hScrollView = new HorizontalScrollView(context);
       hScrollView.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       
       hScrollView.addView(llHsMain);
       
       llRoot.addView(btnPre);
       llRoot.addView(hScrollView);
       llRoot.addView(btnNext);

       btnPrimary.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setViewsForPrimary();
				reportName = "primary";
			}
		});
       btnSecondary.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				reportName = "secondary";
				setViewsForSecondary();
			}
		}); 
       btnStock.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setViewsForClosingStock();
				
			}
		}); 
       btnConsolidate.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), "Work inprogress", 10).show();
			}
		}); 
       btnDownload.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), "Work inprogress", 10).show();
			}
		}); 
       btnOrders.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), "Work inprogress", 10).show();
			}
		});
       FiltersJSON = getArguments().getString("FiltersJSON");
       distributorID = getArguments().getString("DistributorID");
       try {
		JSONObject json = new JSONObject(FiltersJSON);
		JSONArray arrBrand = json.getJSONArray("brands");
		JSONArray arrProducts = json.getJSONArray("products");
		JSONArray arrCustomers = json.getJSONArray("customers");
		
		JSONArray arrRoutes = json.getJSONArray("routes");
		
		listBrand.clear();
		for(int i=0; i<arrBrand.length(); i++){
			JSONObject obj = arrBrand.getJSONObject(i);
			listBrand.add(obj.getString("brand_name"));
			listBrandID.add(obj.getString("brand_id"));
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(),
		   		android.R.layout.simple_spinner_item, listBrand);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinBrand.setAdapter(dataAdapter);
		listRoutes.clear();		
		listRoutes.add("All");
		listRoutesID.add("0");
		for(int i=0; i<arrRoutes.length(); i++){
			JSONObject obj = arrRoutes.getJSONObject(i);
			listRoutes.add(obj.getString("name"));
			listRoutesID.add(obj.getString("id"));
		}
		ArrayAdapter<String> RouteAdapter = new ArrayAdapter<String>(this.getActivity(),
		   		android.R.layout.simple_spinner_item, listRoutes);
		RouteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinRoute.setAdapter(RouteAdapter);
		
		ArrayList<AutoCompleteItem> productArray = new  ArrayList<AutoCompleteItem>();
		for(int i=0; i<arrProducts.length(); i++){
			JSONObject obj = arrProducts.getJSONObject(i);
			productArray.add(new AutoCompleteItem(obj.getString("prod_desc"),obj.getInt("prod_id")));			
		}
		productAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, productArray);
		acProducts.setAdapter(productAdapter);
		acProducts.setOnItemClickListener(new OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View arg1, int pos,
	                long id) {
				selectedProductItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
				selectedProductID =  selectedProductItem.getID();
			}
		});
		
		ArrayList<AutoCompleteItem> customerArray = new  ArrayList<AutoCompleteItem>();
		for(int i=0; i<arrCustomers.length(); i++){
			JSONObject obj = arrCustomers.getJSONObject(i);
			customerArray.add(new AutoCompleteItem(obj.getString("concatname"),obj.getInt("id")));			
		}
		customerAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, customerArray);
		acCustomers.setAdapter(customerAdapter);
		acCustomers.setOnItemClickListener(new OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View arg1, int pos,
	                long id) {
				selectedCustomerItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
				selectedCustomerID =  selectedCustomerItem.getID();
			}
		});
		
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
       
       FilterBrand=(LinearLayout)rootView.findViewById(R.id.FilterBrand);
       FilterProduct=(LinearLayout)rootView.findViewById(R.id.FilterProduct);
       FilterDateRange=(LinearLayout)rootView.findViewById(R.id.FilterDateRange);
       FilterRoute=(LinearLayout)rootView.findViewById(R.id.FilterRoute);
       FilterCustomer=(LinearLayout)rootView.findViewById(R.id.FilterCustomer);
       HideAllFilters();	
       setViewsForPrimary();
        return rootView;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
	    case -1: // Previous Button Clicked
	    	
	        hScrollView.scrollBy(-scrWidth, 0); // see -ve sign to scroll back horizontally only scrWidth is 80% of Total Screen
	        break;
	    case 0: // Next Button Clicked
	        hScrollView.scrollBy(scrWidth, 0); // scrolls next horizontally only scrWidth is 80% of Total Screen
	        break;

	    case 1: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 1 Clicked",1).show();
	        break;
	    case 2: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 2 Clicked",1).show();
	        break;
	    case 3: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 3 Clicked",1).show();
	        break;
	    case 4: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 4 Clicked",1).show();
	        break;
	    case 5: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 5 Clicked",1).show();
	        break;
	        // repeat up to 6

	    case 6:
	        Toast.makeText(v.getContext(), "Menu 6 Clicked",1).show();
	        break;
	    default:
	        break;
	    }
	}
	
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        String strDate = date.getText().toString();
        String[] date_split = strDate.split("/");
        int yy = Integer.parseInt(date_split[2]);
        int mm = Integer.parseInt(date_split[1])-1;
        int dd = Integer.parseInt(date_split[0]);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm+1, dd);
        }
        public void populateSetDate(int year, int month, int day) {            
            date.setText(day+"/"+month+"/"+year);
        }
    }
	
	public void setViewsForPrimary(){
		apiname="";
		reportName = "primary";
		HideAllFilters();
		FilterDateRange.setVisibility(View.VISIBLE);
	}
	
	public void setViewsForSecondary(){
		reportName = "secondary";
		apiname ="getSecondarySales";
		HideAllFilters();
		FilterBrand.setVisibility(View.VISIBLE); 
		FilterProduct.setVisibility(View.VISIBLE); 
		FilterDateRange.setVisibility(View.VISIBLE); 
		FilterRoute.setVisibility(View.VISIBLE); 
		FilterCustomer.setVisibility(View.VISIBLE);
	}
	
	public void setViewsForClosingStock(){
		reportName = "secondary";
		apiname = "getClosingStock";
		HideAllFilters();		
		FilterBrand.setVisibility(View.VISIBLE);
	}
	public void HideAllFilters(){
		FilterBrand.setVisibility(View.GONE);
		FilterProduct.setVisibility(View.GONE); 
		FilterDateRange.setVisibility(View.GONE); 
		FilterRoute.setVisibility(View.GONE); 
		FilterCustomer.setVisibility(View.GONE);
		
	}
	
	public boolean verifyDateRange(){
		String startDate = TVStartDate.getText().toString();
		String endDate = TVEndDate.getText().toString();
		Date StartDate, EndDate;
		if(startDate.contentEquals("dd/mm/yyyy") || endDate.contentEquals("dd/mm/yyyy")){
			return false;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat server = new SimpleDateFormat("yyyy-MM-dd");
	    try {
			StartDate = sdf.parse(startDate);
			EndDate = sdf.parse(endDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	    
	    int diff= daysBetween(StartDate, EndDate);
	    if(diff<0){
	    	return false;
	    }
	    
	    daterange_start = server.format(StartDate);
	    daterange_end = server.format(EndDate);
		return true;
	}
	
	public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		System.out.println(response);
		if(label.contentEquals("getsecondary")){
			Bundle args = new Bundle();
			args.putString("result", String.valueOf(response));
			args.putString("DistributorID", distributorID);
			args.putString("report", reportName);
			ResultFragment nextFrag= new ResultFragment();
			nextFrag.setArguments(args);
		     getActivity().getFragmentManager().beginTransaction()
		     .replace(R.id.frame_container, nextFrag)											     
		     .addToBackStack("Result")
		     .commit();
		}else if(label.contentEquals("getprimary")){
			Bundle args = new Bundle();
			args.putString("result", String.valueOf(response));
			args.putString("DistributorID", distributorID);
			args.putString("report", reportName);
			ResultFragment nextFrag= new ResultFragment();
			nextFrag.setArguments(args);
		     getActivity().getFragmentManager().beginTransaction()
		     .replace(R.id.frame_container, nextFrag)											     
		     .addToBackStack("Result")
		     .commit();
		}
		
	     
	}
	
	public void getPrimary(){
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "apimanufacturer"));
		params.add(new BasicNameValuePair("view", "getPrimarySales"));
		params.add(new BasicNameValuePair("daterange_start", daterange_start));
		params.add(new BasicNameValuePair("daterange_end", daterange_end));
		params.add(new BasicNameValuePair("id", distributorID));
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getprimary","Loading");
		getPosts.execute(url);
	}
		
	public void getSecondary(){
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "apimanufacturer"));
		params.add(new BasicNameValuePair("view", apiname));
		params.add(new BasicNameValuePair("daterange_start", daterange_start));
		params.add(new BasicNameValuePair("daterange_end", daterange_end));
		params.add(new BasicNameValuePair("id", distributorID));
		
		
		if(selectedProductItem != null){
			if(acProducts.getText().toString().contentEquals("") && !selectedProductItem.getName().toString().contentEquals("")){
				selectedProductID=0;
			}else if(!acProducts.getText().toString().contentEquals(selectedProductItem.getName().toString())){
				Toast.makeText(getActivity(), "Invalid Product", 10).show();
				return;
			}
		}
		
		if(selectedCustomerItem != null){
			if(acCustomers.getText().toString().contentEquals("") && !selectedCustomerItem.getName().toString().contentEquals("")){
				selectedCustomerID=0;
			}else if(!acCustomers.getText().toString().contentEquals(selectedCustomerItem.getName().toString())){
				Toast.makeText(getActivity(), "Invalid Customer", 10).show();
				return;
			}
		}
		
//		if(selectedCustomerItem!= null){
//			if(!acCustomers.getText().toString().contentEquals(selectedCustomerItem.getName().toString())){
//				selectedCustomerID=0;
//			}
//		}
		
		params.add(new BasicNameValuePair("customer_id", String.valueOf(selectedCustomerID)));
		params.add(new BasicNameValuePair("product_id", String.valueOf(selectedProductID)));
		params.add(new BasicNameValuePair("brand", spinBrand.getSelectedItem().toString()));
		params.add(new BasicNameValuePair("route", spinRoute.getSelectedItem().toString()));		
		params.add(new BasicNameValuePair("brand_id", listBrandID.get(spinBrand.getSelectedItemPosition())));
		params.add(new BasicNameValuePair("route_id", listRoutesID.get(spinRoute.getSelectedItemPosition())));		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getsecondary","Loading");
		getPosts.execute(url);
	}
	
	
	public String getStartDateOfMonth()
	{
		Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH)+1;
        int dd = 01;
        
        return dd+"/"+mm+"/"+yy;
	}
	
	public String getEndDateOfMonth()
	{
		Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH)+1;
        int dd = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
        return dd+"/"+mm+"/"+yy;
	}
}