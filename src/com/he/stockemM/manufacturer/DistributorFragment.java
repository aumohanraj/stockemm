package com.he.stockemM.manufacturer;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.he.stockemM.R;
import com.he.stockemM.lib.AsyncRequest;
import com.he.stockemM.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockemM.lib.CustomListAdapter;
import com.he.stockemM.lib.CustomListItem;
import com.he.stockemM.orders.UpdateOrderFragment;

public class DistributorFragment extends Fragment implements OnAsyncRequestComplete{
	
	public DistributorFragment(){}
	private ListView listView;
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private CustomListAdapter adapter;
	String selectedID;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_distributor, container, false);
        listView = (ListView)rootView.findViewById(R.id.listSelectDistributor);
        adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				getDistributorFilters(selectedItem.getID());
			}
		});
		getDistributors();
        return rootView;
    }
	
	public void getDistributors(){
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "apimanufacturer"));
		params.add(new BasicNameValuePair("view", "getSalesmanDistributors"));
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getDisributors","Loading distributors");
		getPosts.execute(url);
	}
	
	public void getDistributorFilters(String id){
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "apimanufacturer"));
		params.add(new BasicNameValuePair("view", "getDistributorFilters"));
		params.add(new BasicNameValuePair("id", id));
		selectedID=id;
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getDisributorFilters","Loading");
		getPosts.execute(url);
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		if(label.contentEquals("getDisributorFilters")){
						
			Bundle args = new Bundle();
			args.putString("FiltersJSON", String.valueOf(response));
			args.putString("DistributorID", selectedID);
			ReportsFragment nextFrag= new ReportsFragment();
			nextFrag.setArguments(args);
		     getActivity().getFragmentManager().beginTransaction()
		     .replace(R.id.frame_container, nextFrag)											     
		     .addToBackStack("Reports")
		     .commit();
			return;
		}
		try {
			JSONArray arrDistributors = new JSONArray(response);
			customList.clear();
			for(int i=0; i<arrDistributors.length(); i++){
				JSONObject obj = arrDistributors.getJSONObject(i);
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(obj.getString("name"));
				listItem.setDescription(obj.getString("street_address"));
				listItem.setID(obj.getString("dist_id"));				
				customList.add(listItem);
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
