package com.he.stockemM.manufacturer;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockemM.R;

public class ResultFragment extends Fragment {
	
	public ResultFragment(){}
	Button btnResultDownload;
	String report, DistributorID, result;
	TextView txtTotal, txtCriteria;
	JSONObject obj;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) { 
        View rootView = inflater.inflate(R.layout.fragment_result, container, false); 
        report = getArguments().getString("report");
        DistributorID =getArguments().getString("DistributorID");
        result =getArguments().getString("result");
        txtTotal = (TextView)rootView.findViewById(R.id.txtTotal);
        txtCriteria = (TextView)rootView.findViewById(R.id.txtCriteria);
        
        btnResultDownload = (Button)rootView.findViewById(R.id.btnResultDownload);
        btnResultDownload.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(v.getContext(), "Work inprogress", 10).show();
			}
		});
        txtTotal.setVisibility(View.GONE);
    	txtCriteria.setVisibility(View.GONE);
        try {
			obj = new JSONObject(result);
			JSONArray arrReport = obj.getJSONArray("report");
			if(obj.getBoolean("error")){
				Toast.makeText(this.getActivity(), "Unable to complete the request. Try Again Later", 10).show();
			}
			else if(arrReport.length()==0){
				Toast.makeText(this.getActivity(), "No results for the given search criteria", 10).show();
			}else{
				TableLayout table = (TableLayout)rootView.findViewById(R.id.ResultTableLayout);
		        table.removeAllViews();
		        if(report.contentEquals("primary")){
		        	
			        String[] keys={"InvoiceNo","TotalValue","Date"};
			        String[] col_names={"InvoiceNo","TotalValue","Date"};
			        TableRow header = new TableRow(getActivity());
					header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
							  LayoutParams.WRAP_CONTENT));
					
					header.removeAllViews();  
					
						for(int j=0; j<keys.length;j++){
							String key = (String)keys[j];
							TextView tv = new TextView(getActivity());
							  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
									  LayoutParams.WRAP_CONTENT));
							  tv.setBackgroundResource(R.drawable.cell_shape);
							  tv.setPadding(5, 5, 5, 5);
							  tv.setText(key);
							  header.addView(tv);
						}
						table.addView(header);
						for(int i=0; i<arrReport.length(); i++){
							JSONObject ResultObj = arrReport.getJSONObject(i);
							TableRow row = new TableRow(getActivity());
							  row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
									  LayoutParams.WRAP_CONTENT));
							for(int j=0; j<keys.length;j++){
								String key = (String)keys[j];
								TextView tv = new TextView(getActivity());
								  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
										  LayoutParams.WRAP_CONTENT));
								  tv.setBackgroundResource(R.drawable.cell_shape);
								  tv.setPadding(5, 5, 5, 5);
								  tv.setText(ResultObj.getString(key));
								  row.addView(tv);
							}
							table.addView(row);
						}
					}
		        else{
		        	JSONArray arrCols = obj.getJSONArray("cols");
		        	JSONArray arrCol_keys = obj.getJSONArray("col_keys");
		        	String criteria = obj.getString("criteria");
		        	String summary = obj.getString("summary");
		        	txtCriteria.setText(criteria);
		        	txtTotal.setText(summary);
		        	
		        	txtCriteria.setVisibility(View.VISIBLE);		        	
		        	txtTotal.setVisibility(View.VISIBLE);
		        	
		        	List<String> lCols = new ArrayList<String>();
		        	List<String> lColKeys = new ArrayList<String>();
		        	for(int j=0; j<arrCols.length();j++){
		        		lCols.add(arrCols.getString(j));
		        		lColKeys.add(arrCol_keys.getString(j));
		        	}
		        	

			        TableRow header = new TableRow(getActivity());
					header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
							  LayoutParams.WRAP_CONTENT));
					
					header.removeAllViews();  
					
						for(int j=0; j<lCols.size();j++){
							String key = lCols.get(j);
							TextView tv = new TextView(getActivity());
							  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
									  LayoutParams.WRAP_CONTENT));
							  tv.setBackgroundResource(R.drawable.cell_shape);
							  tv.setPadding(5, 5, 5, 5);
							  tv.setText(key);
							  header.addView(tv);
						}
						table.addView(header);
						for(int i=0; i<arrReport.length(); i++){
							JSONObject ResultObj = arrReport.getJSONObject(i);
							TableRow row = new TableRow(getActivity());
							  row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
									  LayoutParams.WRAP_CONTENT));
							for(int j=0; j<lColKeys.size();j++){
								String key = lColKeys.get(j);
								TextView tv = new TextView(getActivity());
								  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
										  LayoutParams.WRAP_CONTENT));
								  tv.setBackgroundResource(R.drawable.cell_shape);
								  tv.setPadding(5, 5, 5, 5);
								  
								  tv.setText(ResultObj.getString(key));							  
								  row.addView(tv);
							}
							table.addView(row);
						}
					}
			}
			
        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //ScrollView scroll = (ScrollView)rootView.findViewById(R.id.ResultScrollView);
        //HorizontalScrollView hScrollView = (HorizontalScrollView)rootView.findViewById(R.id.ResultHorizontalView);
        
		
		
        return rootView;
    }
}
